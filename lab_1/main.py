from tkinter import Tk, Label, Button, Entry


def encrypt(message, key, symbols_upper, symbols_lower):
    encrypted = ''
    for letter in message:
        if letter.isalpha() is False:
            encrypted += letter
            continue

        if letter.isupper() is True:
            symbols = symbols_upper
        else:
            symbols = symbols_lower
        index = symbols.find(letter) + key
        if index >= len(symbols):
            index = (index % len(symbols))
        encrypted = encrypted + symbols[index]
    return encrypted


def decrypt(message, key, symbols_upper, symbols_lower):
    decrypted = ''
    for letter in message:
        if letter.isalpha() is False:
            decrypted += letter
            continue

        if letter.isupper() is True:
            symbols = symbols_upper
        else:
            symbols = symbols_lower
        index = symbols.find(letter) - key
        if index < len(symbols):
            index = (index % len(symbols))
        decrypted = decrypted + symbols[index]
    return decrypted


def start():
    window = Tk()
    window.title = "Information Security Lab 1"
    window.geometry('1280x720')

    lbl = Label(window, text="Enter source text:", font=("Arial Bold", 50))
    lbl.grid(column=0, row=0)

    txt_source = Entry(window, width=50)
    txt_source.grid(column=0, row=1)
    txt_source.focus()

    lbl = Label(window, text="Enter k:", font=("Arial Bold", 50))
    lbl.grid(column=0, row=2)

    txt_k = Entry(window, width=50)
    txt_k.grid(column=0, row=3)

    lbl_encrypted = Label(window, text="Encrypted text", font=("Arial Bold", 50))
    lbl_encrypted.grid(column=0, row=4)

    lbl_decrypted = Label(window, text="Decrypted text", font=("Arial Bold", 50))
    lbl_decrypted.grid(column=0, row=5)

    def clicked_encrypt():
        symbols_latin_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        symbols_latin_lower = "abcdefghijklmnopqrstuvwxyz"

        source = txt_source.get()
        k = int(txt_k.get())
        r = encrypt(message=source, key=k, symbols_upper=symbols_latin_upper, symbols_lower=symbols_latin_lower)
        lbl_encrypted.configure(text=r)

    def clicked_decrypt():
        symbols_latin_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        symbols_latin_lower = "abcdefghijklmnopqrstuvwxyz"

        source = txt_source.get()
        k = int(txt_k.get())
        r = decrypt(message=source, key=k, symbols_upper=symbols_latin_upper, symbols_lower=symbols_latin_lower)
        lbl_decrypted.configure(text=r)

    btn_encrypt = Button(window, text="ENCRYPT", command=clicked_encrypt)
    btn_decrypt = Button(window, text="DECRYPT", command=clicked_decrypt)

    btn_encrypt.grid(column=0, row=7)
    btn_decrypt.grid(column=1, row=7)

    window.mainloop()


if __name__ == "__main__":
    start()
